/*===================GLOBAL===================*/
var dsnv = [];

/*===================LẤY LOCAL STORAGE LÊN ===================*/
var listLocalStorage = localStorage.getItem("DSNV");
if (JSON.parse(listLocalStorage)) {
  var data = JSON.parse(listLocalStorage);
  for (var index = 0; index < data.length; index++) {
    var current = data[index];
    var nv = new NhanVien(
      current.taiKhoan,
      current.hoTen,
      current.email,
      current.matKhau,
      current.ngayLam,
      current.luongCB,
      current.chucVu,
      current.gioLam
    );
    dsnv.push(nv);
  }
}
renderDanhSachNV(dsnv);

/*===================NÚT THÊM NV===================*/
getEl("#btnThemNV").onclick = function () {
  var newNhanVien = layThongTinTuForm();

  // Validate tài khoản
  var isValid =
    validator.kiemTraRongInput(
      newNhanVien.taiKhoan,
      "#tbTKNV",
      "Tài khoản không được để rỗng"
    ) && validator.kiemTraDoDaiInput(newNhanVien.taiKhoan, "#tbTKNV", 4, 6);

  // Validate tên
  isValid =
    isValid &
      validator.kiemTraRongInput(
        newNhanVien.hoTen,
        "#tbTen",
        "Họ tên không được để rỗng"
      ) &&
    validator.kiemTraChuInput(
      newNhanVien.hoTen,
      "#tbTen",
      "Họ tên phải là chữ"
    );

  // Validate email
  isValid =
    isValid &
      validator.kiemTraRongInput(
        newNhanVien.email,
        "#tbEmail",
        "Email không được để rỗng"
      ) &&
    validator.kiemTraEmailInput(
      newNhanVien.email,
      "#tbEmail",
      "Email không hợp lệ"
    );

  // Validate password
  isValid =
    isValid &
      validator.kiemTraRongInput(
        newNhanVien.matKhau,
        "#tbMatKhau",
        "Mật Khẩu không được để rỗng"
      ) &&
    validator.kiemTraMatKhauInput(
      newNhanVien.matKhau,
      "#tbMatKhau",
      "Mật khẩu không hợp lệ"
    );

  // Validate ngày làm
  isValid =
    isValid &
      validator.kiemTraRongInput(
        newNhanVien.ngayLam,
        "#tbNgay",
        "Ngày làm không được để rỗng"
      ) &&
    validator.kiemTraNgayInput(
      newNhanVien.ngayLam,
      "#tbNgay",
      "Ngày làm không hợp lệ"
    );

  // Validate tiền lương
  isValid =
    isValid &
      validator.kiemTraRongInput(
        newNhanVien.luongCB,
        "#tbLuongCB",
        "Lương không được để rỗng"
      ) &&
    validator.kiemTraLuongInput(
      newNhanVien.luongCB,
      "#tbLuongCB",
      1000000,
      20000000
    );

  // Validate giờ làm
  isValid =
    isValid &
    validator.kiemTraRongInput(
      newNhanVien.gioLam,
      "#tbGiolam",
      "Giờ làm không được để rỗng"
    );

  if (isValid == false) {
    return;
  }

  dsnv.push(newNhanVien);
  saveLocalStorage(dsnv);
  renderDanhSachNV(dsnv);
};

/*===================NÚT XÓA NV===================*/
function xoaNV(id) {
  console.log("id: ", id);
  var index = timViTri(id, dsnv);
  if (index !== -1) {
    dsnv.splice(index, 1);
    saveLocalStorage(dsnv);
    renderDanhSachNV(dsnv);
  }
}

/*===================NÚT SỬA NV===================*/
function suaNV(id) {
  var index = timViTri(id, dsnv);
  if (index !== -1) {
    var sv = dsnv[index];
    showThongTinLenForm(sv);
    getEl("#tknv").disabled = true;
  }
}

/*===================NÚT CẬP NHẬT NV===================*/
getEl("#btnCapNhat").onclick = function () {
  // validate họ tên
  var isValid =
    validator.kiemTraRongInput(
      nvEdited.hoTen,
      "#tbTen",
      "Họ tên không được để rỗng"
    ) &&
    validator.kiemTraChuInput(nvEdited.hoTen, "#tbTen", "Họ tên phải là chữ");

  // validate email
  isValid =
    isValid &
      validator.kiemTraRongInput(
        nvEdited.email,
        "#tbEmail",
        "Email không được để rỗng"
      ) &&
    validator.kiemTraEmailInput(
      nvEdited.email,
      "#tbEmail",
      "Email không hợp lệ"
    );

  // validate mật khẩu
  isValid =
    isValid &
      validator.kiemTraRongInput(
        nvEdited.matKhau,
        "#tbMatKhau",
        "Mật Khẩu không được để rỗng"
      ) &&
    validator.kiemTraMatKhauInput(
      nvEdited.matKhau,
      "#tbMatKhau",
      "Mật khẩu không hợp lệ"
    );

  // Validate ngày làm
  isValid =
    isValid &
      validator.kiemTraRongInput(
        nvEdited.ngayLam,
        "#tbNgay",
        "Ngày làm không được để rỗng"
      ) &&
    validator.kiemTraNgayInput(
      nvEdited.ngayLam,
      "#tbNgay",
      "Ngày làm không hợp lệ"
    );

  // Validate tiền lương
  isValid =
    isValid &
      validator.kiemTraRongInput(
        nvEdited.luongCB,
        "#tbLuongCB",
        "Lương không được để rỗng"
      ) &&
    validator.kiemTraLuongInput(
      nvEdited.luongCB,
      "#tbLuongCB",
      1000000,
      20000000
    );

  // Validate giờ làm
  isValid =
    isValid &
    validator.kiemTraRongInput(
      nvEdited.gioLam,
      "#tbGiolam",
      "Giờ làm không được để rỗng"
    );
  if (isValid == false) {
    return;
  }
  var nvEdited = layThongTinTuForm();
  var index = timViTri(nvEdited.taiKhoan, dsnv);
  if (index !== -1) {
    dsnv[index] = nvEdited;
    renderDanhSachNV(dsnv);
    saveLocalStorage(dsnv);
  }
};

/*===================NÚT RESET===================*/
getEl("#btnReset").onclick = function () {
  resetLogin();
  getEl("#tknv").disabled = false;
};

/*===================CHỨC NĂNG SEARCH===================*/
function searchLoaiNV() {
  var searchLoaiInput = getEl("#searchName").value;
  var searchLoaiNV = dsnv.filter(function (element) {
    return element
      .xepLoai()
      .toUpperCase()
      .includes(searchLoaiInput.toUpperCase());
  });
  if ((searchLoaiInput = searchLoaiNV)) {
    renderDanhSachNV(searchLoaiNV);
  }
  if ((searchLoaiInput = "")) {
    renderDanhSachNV(dsnv);
  }
}
