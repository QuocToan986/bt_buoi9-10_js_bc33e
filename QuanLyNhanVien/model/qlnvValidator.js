var validator = {
  kiemTraRongInput: function (valueInput, idError, message) {
    if (valueInput == "") {
      getEl(idError).innerText = message;
      return false;
    } else {
      getEl(idError).innerText = "";
      return true;
    }
  },
  kiemTraDoDaiInput: function (valueInput, idError, min, max) {
    var inputLength = valueInput.length;
    if (inputLength < min || inputLength > max) {
      getEl(idError).innerHTML = ` Độ dài từ ${min} - ${max} ký tự `;
      return false;
    } else {
      getEl(idError).innerText = "";
      return true;
    }
  },
  kiemTraChuInput: function (valueInput, idError, message) {
    var letters =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (valueInput.match(letters)) {
      getEl(idError).innerText = "";
      return true;
    } else {
      getEl(idError).innerText = message;
      return false;
    }
  },
  kiemTraEmailInput: function (valueInput, idError, message) {
    var emailRegex = /\S+@\S+\.\S+/;
    if (valueInput.match(emailRegex)) {
      getEl(idError).innerText = "";
      return true;
    } else {
      getEl(idError).innerText = message;
      return false;
    }
  },
  kiemTraMatKhauInput: function (valueInput, idError, message) {
    var matKhauRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9#?!@$%^&*-]).{6,}$/;
    if (valueInput.match(matKhauRegex)) {
      getEl(idError).innerText = "";
      return true;
    } else {
      getEl(idError).innerText = message;
      return false;
    }
  },
  kiemTraNgayInput: function (valueInput, idError, message) {
    var ngayRegex =
      /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
    if (valueInput.match(ngayRegex)) {
      getEl(idError).innerText = "";
      return true;
    } else {
      getEl(idError).innerText = message;
    }
  },
  kiemTraLuongInput: function (valueInput, idError, min, max) {
    if (valueInput < min || valueInput > max) {
      getEl(idError).innerHTML = `Lương từ ${min} - ${max} triệu `;
      return false;
    } else {
      getEl(idError).innerText = "";
      return true;
    }
  },
  kiemTraGioLamInput: function (valueInput, idError, min, max) {
    if (valueInput * 1 < min || valueInput * 1 > max) {
      getEl(
        idError
      ).innerHTML = `Tổng giờ làm trong tháng ${min} - ${max} giờ `;
      return false;
    } else {
      getEl(idError).innerText = "";
    }
  },
};
