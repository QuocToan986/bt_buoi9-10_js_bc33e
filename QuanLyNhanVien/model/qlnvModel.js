function NhanVien(
  taiKhoanNV,
  hoTenNV,
  emailNV,
  matKhauNV,
  ngayLamNV,
  luongCBNV,
  chucVuNV,
  gioLamNV
) {
  this.taiKhoan = taiKhoanNV;
  this.hoTen = hoTenNV;
  this.email = emailNV;
  this.matKhau = matKhauNV;
  this.ngayLam = ngayLamNV;
  this.luongCB = luongCBNV;
  this.chucVu = chucVuNV;
  this.gioLam = gioLamNV;

  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luongCB * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luongCB * 2;
    } else if (this.chucVu == "Nhân viên") {
      return this.luongCB;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Xuất sắc";
    } else if (this.gioLam >= 167) {
      return "Giỏi";
    } else if (this.gioLam >= 160) {
      return "Khá";
    } else {
      return "Trung bình ";
    }
  };
}
