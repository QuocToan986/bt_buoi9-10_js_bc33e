/*===================HÀM DOM===================*/
function getEl(selector) {
  return document.querySelector(selector);
}

/*===================HÀM LẤY THÔNG TIN TỪ FORM===================*/
function layThongTinTuForm() {
  var taiKhoanNV = getEl("#tknv").value.trim();
  var hoTenNV = getEl("#name").value.trim();
  var emailNV = getEl("#email").value.trim();
  var matKhauNV = getEl("#password").value.trim();
  var ngayLamNV = getEl("#datepicker").value.trim();
  var luongCBNV = getEl("#luongCB").value.trim() * 1;
  var chucVuNV = getEl("#chucvu").value;
  var gioLamNV = getEl("#gioLam").value.trim() * 1;

  var nhanVien = new NhanVien(
    taiKhoanNV,
    hoTenNV,
    emailNV,
    matKhauNV,
    ngayLamNV,
    luongCBNV,
    chucVuNV,
    gioLamNV
  );
  return nhanVien;
}

/*===================HÀM LƯU XUỐNG LOCAL===================*/
function saveLocalStorage(arr) {
  var listJson = "";
  return (
    (listJson = JSON.stringify(arr)), localStorage.setItem("DSNV", listJson)
  );
}

/*===================HÀM RENDER TABLE===================*/
function renderDanhSachNV(arr) {
  var contentHTML = "";
  arr.forEach(function (item) {
    var content = `
    <tr>
    <td>${item.taiKhoan}</td>
    <td>${item.hoTen}</td>
    <td>${item.email}</td>
    <td>${item.ngayLam}</td>
    <td>${item.chucVu}</td>
    <td>${item.tongLuong()}</td>
    <td>${item.xepLoai()}</td>
    <td>
        <button class = "btn-primary" onclick = "suaNV('${
          item.taiKhoan
        }')">Sửa</button>
        <button class = "btn-danger" onclick = "xoaNV('${
          item.taiKhoan
        }')">Xóa</button>
    </td>
    </tr>
    `;
    contentHTML += content;
  });
  getEl("#tableDanhSach").innerHTML = contentHTML;
}

/*===================HÀM TÌM VỊ TRÍ===================*/
function timViTri(id, arr) {
  for (var index = 0; index < arr.length; index++) {
    var nhanVien = arr[index];
    if (nhanVien.taiKhoan == id) {
      return index;
    }
  }
  return -1;
}

/*===================HÀM LẤY THÔNG TIN LÊN FORM===================*/
function showThongTinLenForm(nv) {
  getEl("#tknv").value = nv.taiKhoan;
  getEl("#name").value = nv.hoTen;
  getEl("#email").value = nv.email;
  getEl("#password").value = nv.matKhau;
  getEl("#datepicker").value = nv.ngayLam;
  getEl("#luongCB").value = nv.luongCB;
  getEl("#chucvu").value = nv.chucVu;
  getEl("#gioLam").value = nv.gioLam;
}

/*===================HÀM RESET LOGIN===================*/
function resetLogin() {
  getEl("#tknv").value = "";
  getEl("#name").value = "";
  getEl("#email").value = "";
  getEl("#password").value = "";
  getEl("#luongCB").value = "";
  getEl("#gioLam").value = "";
}
